## OpenShift auto-deployments (GitLab CI)

This is the repository that builds Docker Image with all scripts needed to
deploy to OpenShift from GitLab CI.

It basically consist of three stages:
1. Build stage where a Docker Image is built,
2. Testing stage is a place holder for Bendigo DevOps to test the images,
3. Deploy stage where a previously built Docker Image is run on OpenShift and
   exposed on hostname from a helm chart.

### Build stage

The build script does:
1. Check if the repository has `Dockerfile`,
2. If yes, use `docker build` to build Docker Image,
3. Login to GitLab Container Registry,
4. Push build image to GitLab Container Registry.

### Testing stage

1. This part is for Bendigo DevOps this to inject the necessary tests,
2. It is a placeholder for expansion,
3. It can easily be modified with testing functions,

### Deploy stage

The deploy script does:
1. Create or upgrades the application with the helm chart, `helm updrage --install`command
2. Automatically archives the previous helm deployment, with rollback feature
3. Deploys the application with most recent Docker Image and helm chart setting,
4. Expose route/ingress with given hostname for this application.

### Requirements

1. GitLab Runner using Docker or Kubernetes executor with privileged mode enabled,
2. Service account for existing OpenShift cluster,
3. DNS wildcard domain to host deployed applications.

### Limitations

1. Only private docker images can be deployed,
2. This is entirely managed by DevOps, very little involvement from developers,
3. There is no ability to pass environment variables to deployed application,
4. Additional instrumentation is required after deploying image on OCP from helm charts

### Troubleshooting

Ensure that you're using the `helm` resources  instead of `kubectl` Deployment Configuration in order to render correctly.


### Examples

You can see existing working examples:
1.

### How to use it?

Basically, configure Kubernetes Service in your project settings and
copy-paste [this `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/autodeploy/OpenShift.gitlab-ci.yml).
